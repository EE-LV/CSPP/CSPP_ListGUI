CS++ ListGUI README
===================
This is the ListGUI package for CS++. The ListGUI is an additional GUI designed for devices with different
channels. 

It aims:
- for an easy visualization of the channel information and status.
- for a fast and intuitive way to control these channels. There are no buttons; values will be directly entered into the cells (like Excel). The right mouse button can open an additional menu for boolean commands.
- to be easy configurable to use it with different types of devices (i.e. power supplies, function generators, step motors, etc.).
- to be used as Sub-panel in larger, application GUIs.

The package includes the GUI itself as an actor within *...\Actors\CSPP_ListGui* and the so called channel-data found in *...\Actors\CSPP_ListGuiChannelData*.
It should not be necessary to touch the GUI itself unless new features are to be implemented. 
New configuration schemes can be created by adding new classes within the *CSPP_ListGuiChannelData* folder and inherit from the *CSPP_BaseChannelData* class which can be found at *...\Actors\CSPP_ListGuiChannelData\Base*.

![An example of the ListGUI for a power supply with eight channels](Docs/listgui_example.png)

Usage of the ListGUI
--------------------
To use the ListGUI with an already existing configuration first the CS++ ListGUI package has to be downloaded and installed and then some modifications have to
be made within the Ini-file. Like for all GUIs within CS++ the GUI information are either directly connected to a specific device actor or an additional database
entry has to be made for the GUI:

- First the ListGUI has to be chosen as GUI for this specific actor:
```
CSPP_BaseActor:CSPP_BaseActor.DefaultGUI="CSPP_ListGui.lvlib:CSPP_ListGui.lvclass"
```
- Second a ListGUI configuration has to be chosen. For example:
```
CSPP_ListGui:CSPP_ListGui.TypeClassPath="CSPP_ListGui.lvlib:CSPP_HVChannelData.lvclass"
```
- Third it can be chosen whether a user menu should be shown or not:
```
CSPP_ListGui:CSPP_ListGui.ShowFileMenu=True
```

So for a GUI directly connected to a specific device an example Ini-entry could look like this:
```
[myISEG2]
LVClassPath="ISEG.lvlib:ISEG.lvclass"
CSPP_BaseActor:CSPP_BaseActor.DefaultGUI="CSPP_DCPwrGui.lvlib:CSPP_DCPwrGui.lvclass"
CSPP_BaseActor:CSPP_BaseActor.Settings="Auto"
CSPP_BaseActor:CSPP_BaseActor.LaunchDefaultGUI=False
CSPP_BaseActor:CSPP_BaseActor.ErrorDialog=True
CSPP_BaseActor:CSPP_BaseActor.PVProxy="myISEG2Proxy"
CSPP_BaseActor:CSPP_BaseActor.LaunchPVProxy=True
CSPP_BaseActor:CSPP_BaseActor.AllURLsOptional=False
CSPP_DeviceActor:CSPP_DeviceActor.ResourceName="//localhost/ISEG/OPC/can0"
CSPP_DeviceActor:CSPP_DeviceActor.Reset=False
CSPP_DeviceActor:CSPP_DeviceActor.Selftest=False
CSPP_DeviceActor:CSPP_DeviceActor.OptionString="Simulate=0,RangeCheck=1,QueryInstrStatus=0,Cache=1"
CSPP_DeviceActor:CSPP_DeviceActor.ResetWithDefaults=False
CSPP_DCPwr:CSPP_DCPwr.ChannelNames="0,1,2,3,4,5,6,7"
CSPP_ListGui:CSPP_ListGui.TypeClassPath="CSPP_ListGui.lvlib:CSPP_HVChannelData.lvclass"
CSPP_ListGui:CSPP_ListGui.ShowFileMenu=True
...
```

An example for a GUI with a separate Ini-entry is:
```
[myISEG2GUI]
LVClassPath="CSPP_ListGui.lvlib:CSPP_ListGui.lvclass"
CSPP_GUIActor:CSPP_GUIActor.AssociatedActors="myISEG2"
CSPP_GUIActor:CSPP_GUIActor.AssociatedPVProxys="myISEG2Proxy"
CSPP_GUIActor:CSPP_GUIActor.StopAssociatedActor=True
CSPP_DCPwr:CSPP_DCPwr.ChannelNames_myISEG2="0,1,2,3,4,5,6,7"
CSPP_ListGui:CSPP_ListGui.TypeClassPath="CSPP_ListGui.lvlib:CSPP_HVChannelData.lvclass"
CSPP_ListGui:CSPP_ListGui.ShowFileMenu=True
```
In this example note that the list with the channel names has to be entered here as well. Such a "disconnected" GUI can also subscribe only to a subset of available channels.

Creation of new ListGUI configurations
--------------------------------------
The creation of new GUI configurations is rather simple. One just has to create a new ChannelData class, which inherits from *CSPP_BaseChannelData*. Afterwards one has to override
three specific VIs:

![ListGUI in the Project Explorer. Shown are the three configuration VIs for the HVChannel class.](Docs/listgui_channel_example.png)

1. **Initialize Attributes Core.vi**: This VI is executed during the startup of the GUI. One needs to enter most of the configuration parameters here.

![Example of a Initialize Attributes Core.vi for the FGen class.](Docs/listgui_initattribute.png)

There are five different positions this VI needs to be modified:
  * A. *DB-Entry: Channel Names:* The GUI needs to know the name of the Ini-file entry for the channel names (and channel multiplier names). Very often this depends on the base class of the device.
  * B. *PV-Infos (per column)*: Each entry in this list represents one column of the ListGUI and for each column one can configure here the connections to the PVs as well as the
  overall behavior of the data shown. Each entry has three sub-clusters: Monitor-PV, Set-PV and GUI-TypeDescriptor:
    * Monitor-PV: One just has to enter the name of the PV which should be shown in this column (so "Set_Voltage", "Get_Voltage", "Set_Frequency", etc) as well as the TypeDescriptor of the
	PV, a boolean indicating if there is always the channel name appended to the PV name and a number indicating if this channel name has to be modified due to a possible (and then fixed for each column) channel multiplier.
	An example for a PV without a channel appended to its name is "Device_Ressource" (because it is most probable identical for all channels). The "Get_Voltage" is an example for a PV which needs a
	channel suffix. And for switches one might need two "Set_Voltages" for each channel: "Set_Voltage_0L" and "Set_Voltage_0H" depending if the TTL input of this channel is low or high. Note that "-1" indicates that this
	PV name does not have to be modified due to channel multipliers.
    * Set-PV: It is also possible to not only show actual values of specific PVs but also modify them by the user. For this one first has to set the first boolean in the Set-PV cluster to TRUE.
	Afterwards one can set message name of the PV used to send a new value to the device actor as well as its Type Descriptor. 
	* GUI-TypeDescriptor: The last cluster defines the data type shown on the GUI within this column. For this DataType classes are used which can be found in *..\Actors\CSPP_ListGuiTypeDefs*.
	All these data types result in slightly different cell behaviors (allow negative values or doubles etc). One can also use the special data type *Enum*. This data type creates an enum on the GUI. The entries of this
	enum have to be either entered directly in the list within this cluster, or better one can give a path to a specific LabVIEW control separated by a blank (see example above). Already implemented TypeDefs can be found below.
  * C. *Set-PVs (per context menu entry)*: This list configures the menu which pops up if the user highlights at least one channel and clicks on the right mouse button. It can be used
  to switch on and off channels for example. The entries of this list are first the name of the entry in the list, then the name and TypeDef of the connected set-PV, and then a list with fixed
  parameters, which are set in a second for loop in this example. 
  * D. *Column-Headers*: A list with the column headers. Nothing more to say...
  * E. *Set Default-Values for each column*: One has to set default values for the columns and convert them to variants here. Just be aware of the datatype needed for each column.
2. **Data 2 FP Core.vi**: This VI parses the information received from the PVs to the front panel of the GUI.

![Example of a Data 2 FP Core.vi for the HVChannel class.](Docs/listgui_data2fp.png)

Always use the same structure like in the example VIs. If a new configuration should be created two tasks have to be done in this VI:
  * Convert the data from the 2D Variant array into a 2D string array, which can be shown on the front panel. This should be easy. The column numbers and there data types are known at this stage.
  * For each row the status can be calculated here. Just choose the right color code from the 2D color code array. Depending on the type of the channel and device one might want to check the output
  status and the difference between a Get and Set PV here.
3. **SendValueToDevice Core.vi**: This VI will be executed once the user enters a new value which should be send to the device.

![Example of a SendValueToDevice Core.vi for the HVChannel class.](Docs/listgui_sendvalue2device.png)

Just convert the value coming within the variant into a format which the Set-PV can understand. Also send the name of the channel together with the value inside a cluster if necessary.

| ListGUI TypeDef | allowed chars by user | allowed negative values |
| --------------- | --------------------- | ----------------------- |
| Int             | Numbers, Minus        | yes                     |
| PosInt          | Numbers               | no, negative become 0   |
| Dbl             | Numbers, Minus, Dot   | yes                     |
| PosDbl          | Numbers, Dot          | no, negative become 0   |
| Enum            | N/A                   | N/A                     |

Multi-Device operation of ListGUI
---------------------------------
It is also possible to use one GUI to control channels from different devices of the same type. This is especially helpful for frequency generators, which have very often
only one or two channels. The ListGUI can be configured so that only one GUI can control the channels of as many devices as needed.

Please note that from the architecture point of view this means that the ListGUI moves now to the application layer. This has for example some implications on the Load/Save process (then the GUI cannot simply
send save and load commands to the device, it has to save and load the values by itself).

The next example shows an Ini-file configuration to control 16 channels of two power supplies:
```
[myISEGTestGUI]
LVClassPath="CSPP_ListGui.lvlib:CSPP_ListGui.lvclass"
CSPP_GUIActor:CSPP_GUIActor.AssociatedActors="myISEG,myISEG2"
CSPP_GUIActor:CSPP_GUIActor.AssociatedPVProxys="myISEGProxy,myISEG2Proxy"
CSPP_GUIActor:CSPP_GUIActor.StopAssociatedActor=True
CSPP_DCPwr:CSPP_DCPwr.ChannelNames_myISEG="0,1,2,3,4,5,6,7"
CSPP_DCPwr:CSPP_DCPwr.ChannelNames_myISEG2="0,1,2,3,4,5,6,7"
CSPP_ListGui:CSPP_ListGui.TypeClassPath="CSPP_ListGui.lvlib:CSPP_HVChannelData.lvclass"
CSPP_ListGui:CSPP_ListGui.ShowFileMenu=True
```
So one just has to enter lists with channel names for all devices and the names of these devices and the corresponding proxies.
The result is:
![An example of a ListGUI controlling two power supplies](Docs/listgui_grouped_example.png)

Available ListGUI configurations
--------------------------------
There are already several configurations for the ListGUI available:

| ListGUI Type | device type | compatible device classes | table columns (Set variables in bold) |
| --------     | ----------- | ------------------------- | ------------- |
| HVChannelData | PowerSupply | CAEN, ISEG                | **Set (V)**, Measure (V), **Set (uA)**, Measure (uA) |
| GenesysChannelData | PowerSupply | LambdaGenesys   | **Set (V)**, Measure (V), **Set (A)**, Measure (A) |
| HVSwitchChannelData | PowerSupply | GSI-HVSwitch2, HD-Switch | **Set_Low (V)**, **Set_High (V)**, Measure (V) |
| FGenStandard | FunctionGenerator | Agilent33XXX, Agilent3352X | **Amplitude (V)**, **Frequency (Hz)**, **DCOffset (V)**, **WfmType**, BurstMode?, **Phase (deg)**, **BurstCounts** |

An additional class for the PSConnect device class will not be explained here.

Feature Requests and Bug reports
---------------------------------
All feature requests and bug reports are collected  within GitLab. Please use this [web-page](https://git.gsi.de/EE-LV/CSPP/CSPP_ListGUI/issues).

Related information
---------------------------------
Author: D.Neidherr@gsi.de

Copyright 2020  GSI Helmholtzzentrum für Schwerionenforschung GmbH

Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: http://www.osor.eu/eupl

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.