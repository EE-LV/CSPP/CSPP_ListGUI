﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="21008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2017_data</Property>
	<Property Name="Alarm Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2017\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2017_data</Property>
	<Property Name="Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2017\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="NI.Lib.Description" Type="Str">This library contains a List GUI which can be used by several CS++ device actors.

Author: H.Brand@gsi.de, D.Neidherr@gsi.de

Copyright 2013  GSI Helmholtzzentrum für Schwerionenforschung GmbH

Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: http://www.osor.eu/eupl

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"=&gt;MQ%!8143;(8.6"2CVM#WJ",7Q,SN&amp;(N&lt;!NK!7VM#WI"&lt;8A0$%94UZ2$P%E"Y.?G@I%A7=11U&gt;M\7P%FXB^VL\`NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAG_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y!#/7SO!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">553680896</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">)1#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="ChannelData" Type="Folder">
		<Item Name="DCPwr" Type="Folder">
			<Item Name="CSPP_GenesysChannelData.lvclass" Type="LVClass" URL="../../CSPP_ListGuiChannelData/Genesys/CSPP_GenesysChannelData.lvclass"/>
			<Item Name="CSPP_GSI-HVSwitchChannelData.lvclass" Type="LVClass" URL="../../CSPP_ListGuiChannelData/GSI-HVSwitch/CSPP_GSI-HVSwitchChannelData.lvclass"/>
			<Item Name="CSPP_HVChannelData.lvclass" Type="LVClass" URL="../../CSPP_ListGuiChannelData/HVChannel/CSPP_HVChannelData.lvclass"/>
			<Item Name="CSPP_HVChannelData_mA.lvclass" Type="LVClass" URL="../../CSPP_ListGuiChannelData/HVChannel_mA/CSPP_HVChannelData_mA.lvclass"/>
			<Item Name="CSPP_HVChannelData_uA.lvclass" Type="LVClass" URL="../../CSPP_ListGuiChannelData/HVChannel_uA/CSPP_HVChannelData_uA.lvclass"/>
			<Item Name="CSPP_HVSwitchChannelData.lvclass" Type="LVClass" URL="../../CSPP_ListGuiChannelData/HVSwitch/CSPP_HVSwitchChannelData.lvclass"/>
			<Item Name="CSPP_ISEG_THQ.lvclass" Type="LVClass" URL="../../CSPP_ListGuiChannelData/ISEG_THQ/CSPP_ISEG_THQ.lvclass"/>
			<Item Name="CSPP_PSConnectChannelData.lvclass" Type="LVClass" URL="../../CSPP_ListGuiChannelData/PSConnect/CSPP_PSConnectChannelData.lvclass"/>
			<Item Name="CSPP_SimpleHV.lvclass" Type="LVClass" URL="../../CSPP_ListGuiChannelData/SimpleHV/CSPP_SimpleHV.lvclass"/>
			<Item Name="CSPP_StahlPSChannelData.lvclass" Type="LVClass" URL="../../CSPP_ListGuiChannelData/StahlPS/CSPP_StahlPSChannelData.lvclass"/>
		</Item>
		<Item Name="FGen" Type="Folder">
			<Item Name="CSPP_FGenStandard.lvclass" Type="LVClass" URL="../../CSPP_ListGuiChannelData/FGenStandard/CSPP_FGenStandard.lvclass"/>
			<Item Name="ST_FGen.lvclass" Type="LVClass" URL="../../CSPP_ListGuiChannelData/ST_FGen/ST_FGen.lvclass"/>
		</Item>
		<Item Name="Others" Type="Folder">
			<Item Name="CGC_DPC.lvclass" Type="LVClass" URL="../../CSPP_ListGuiChannelData/CGC_DPC/CGC_DPC.lvclass"/>
		</Item>
		<Item Name="CSPP_BaseChannelData.lvclass" Type="LVClass" URL="../../CSPP_ListGuiChannelData/Base/CSPP_BaseChannelData.lvclass"/>
	</Item>
	<Item Name="TypeDefs" Type="Folder">
		<Item Name="Numbers" Type="Folder">
			<Item Name="Dbl.lvclass" Type="LVClass" URL="../../CSPP_ListGuiTypeDefs/Dbl/Dbl.lvclass"/>
			<Item Name="Int.lvclass" Type="LVClass" URL="../../CSPP_ListGuiTypeDefs/Int/Int.lvclass"/>
			<Item Name="PosDbl.lvclass" Type="LVClass" URL="../../CSPP_ListGuiTypeDefs/PosDbl/PosDbl.lvclass"/>
			<Item Name="PosInt.lvclass" Type="LVClass" URL="../../CSPP_ListGuiTypeDefs/PosInt/PosInt.lvclass"/>
		</Item>
		<Item Name="Others" Type="Folder">
			<Item Name="Enum.lvclass" Type="LVClass" URL="../../CSPP_ListGuiTypeDefs/Enum/Enum.lvclass"/>
		</Item>
		<Item Name="CSPP_BaseTypeDef.lvclass" Type="LVClass" URL="../../CSPP_ListGuiTypeDefs/Base/CSPP_BaseTypeDef.lvclass"/>
	</Item>
	<Item Name="CSPP_ListGui.lvclass" Type="LVClass" URL="../CSPP_ListGui.lvclass"/>
</Library>
